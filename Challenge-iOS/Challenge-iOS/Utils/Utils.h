//
//  Utils.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(BOOL)isEmptyString:(NSString *)value;
+(void)showMessage:(UIViewController *)viewController message:(NSString *)message;

@end
