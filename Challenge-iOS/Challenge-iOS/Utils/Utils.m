//
//  Utils.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(BOOL)isEmptyString:(NSString *)value {
    if (!value) {
        return YES;
    } else if ((value == (NSString *) [NSNull null]) || [value isKindOfClass:[NSNull class]]) {
        return YES;
    } else if ([[value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return YES;
    } else {
        return NO;
    }
}

+(void)showMessage:(UIViewController *)viewController message:(NSString *)message {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Atention!" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:ok];

    [viewController presentViewController:alert animated:YES completion:nil];
}

@end
