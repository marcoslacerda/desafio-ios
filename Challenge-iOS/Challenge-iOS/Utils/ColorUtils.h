//
//  ColorUtils.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorUtils : NSObject

+(UIColor *)colorFromHexString:(NSString *)hexString;

@end
