//
//  API.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface API : NSObject

typedef void (^ _Nonnull RequestsResponseErrorBlock) (NSError * _Nullable error, id _Nullable response);

+(void)doLoadUserFullname:(NSString * _Nonnull)username completion:(RequestsResponseErrorBlock)block;

+(void)doLoadRepos:(int)page completion:(RequestsResponseErrorBlock)block;

+(void)doListPullRequests:(NSString * _Nonnull)owner repoName:(NSString * _Nonnull)repoName page:(int)page completion:(RequestsResponseErrorBlock)block;

@end
