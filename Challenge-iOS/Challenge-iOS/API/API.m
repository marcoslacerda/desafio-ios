//
//  API.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "API.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFURLSessionManager.h>

@implementation API

+(void)doLoadRepos:(int)page completion:(RequestsResponseErrorBlock)block {
    NSString *url = [NSString stringWithFormat:@"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%i", page];
    
    [self makeRequestWithPath:url method:GET completion:block];
}

+(void)doLoadUserFullname:(NSString *)username completion:(RequestsResponseErrorBlock)block {
    NSString *url = [NSString stringWithFormat:@"https://api.github.com/users/%@", username];
    
    [self makeRequestWithPath:url method:GET completion:block];
}

+(void)doListPullRequests:(NSString *)owner repoName:(NSString *)repoName page:(int)page completion:(RequestsResponseErrorBlock)block {
    NSString *url = [NSString stringWithFormat:@"https://api.github.com/repos/%@/%@/pulls?page=%i", owner, repoName, page];

    [self makeRequestWithPath:url method:GET completion:block];
}

#pragma mark - Request Configs

+(void)makeRequestWithPath:(NSString *)url method:(kRequestType)requestType completion:(RequestsResponseErrorBlock)block {
    NSURL *request = [NSURL URLWithString:url];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:request];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    // Failure block
    void (^failureBlock)(NSURLSessionDataTask *task, NSError *error) = ^(NSURLSessionDataTask *task, NSError *error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *responseError = nil;
        
        if (errorData) {
            responseError = [NSJSONSerialization JSONObjectWithData:errorData options:kNilOptions error:nil];
        }
        
        if (DEBUG_FLAG) {
            NSLog(@"Request Error: %@", responseError);
        }
        
        block(nil, responseError);
    };
    
    if (requestType == GET) {
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (DEBUG_FLAG) {
                NSLog(@"%@", responseObject);
            }
            
            if (responseObject) {
                block(nil, responseObject);
            }
        } failure:failureBlock];
    }
}

@end
