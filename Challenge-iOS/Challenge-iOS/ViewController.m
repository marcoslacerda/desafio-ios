//
//  ViewController.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "ViewController.h"
#import "RepositoriesTableViewCell.h"
#import "RepositoryModel.h"
#import "RepoDetailViewController.h"
#import <SVPullToRefresh/SVPullToRefresh.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    
    page = 1;
    totalRequested = 0;
    self.title = @"Github JavaPop";
    
    [self loadRepositories:YES];
}

#pragma mark - UI

-(void)initUI {
    [self configureNavigationBar];
    [self configureTableView];
}

-(void)configureNavigationBar {
    self.navigationController.navigationBar.barTintColor = [ColorUtils colorFromHexString:@"#343438"];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
}

-(void)configureTableView {
    [repositoriesTableView registerNib:[UINib nibWithNibName:@"RepositoriesTableViewCell" bundle:nil] forCellReuseIdentifier:@"RepositoriesCell"];
    
    repositoriesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    repositoriesTableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
    
    repositoriesTableView.rowHeight = UITableViewAutomaticDimension;
    repositoriesTableView.estimatedRowHeight = 400;
    
    // Configure Infinite Scroll
    __block id blockSelf = self;
    
    [repositoriesTableView addInfiniteScrollingWithActionHandler:^{
        page = page + 1;
        
        [blockSelf loadRepositories:NO];
    }];
    
    repositoriesTableView.delegate = self;
    repositoriesTableView.dataSource = self;
}

#pragma mark - Actions

-(void)loadRepositories:(BOOL)showProgress {
    if (!items) {
        items = [[NSMutableArray alloc] init];
    }
    
    if (showProgress) {
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }
    
    [API doLoadRepos:page completion:^(NSError * _Nullable error, id  _Nullable response) {
        if (error) {
            [self hideProgress:showProgress];
            [Utils showMessage:self.navigationController message:error.localizedDescription];
        } else if (response) {
            totalRequested += [response[@"items"] count];
            
            if ([response[@"items"] count] == 0) {
                [repositoriesTableView reloadData];
            }
            
            for (NSDictionary *item in response[@"items"]) {
                [RepositoryModel initWithJSON:item completion:^(id  _Nonnull repo) {
                    if (![items containsObject:repo]) {
                        [items addObject:repo];
                    }
                    
                    if (items.count == totalRequested) {
                        [self hideProgress:showProgress];
                        
                        [repositoriesTableView reloadData];
                    }
                }];
            }
        } else {
            [self hideProgress:showProgress];
            [Utils showMessage:self.navigationController message:@"An error occurred to load repositories."];
        }
    }];
}

-(void)hideProgress:(BOOL)performHide {
    if (performHide) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    }
}

-(void)clearItems {
    [items removeAllObjects];
    items = nil;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView.pullToRefreshView stopAnimating];
    [tableView.infiniteScrollingView stopAnimating];
    
    RepositoriesTableViewCell *cell = (RepositoriesTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"RepositoriesCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell initWithItem:items[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositoryModel *repo = items[indexPath.row];
    
    RepoDetailViewController *repoDetailController = [[RepoDetailViewController alloc] init];
    
    repoDetailController.repoDetail = repo;
    
    [self.navigationController pushViewController:repoDetailController animated:YES];
}

@end
