//
//  User.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize avatarURL, userName, fullName;

+(void)initWithJSON:(NSDictionary *)json completion:(void (^)(User *))block {
    User *user = [[User alloc] init];
    
    user.avatarURL = json[@"avatar_url"];
    user.userName = json[@"login"];
    
    // Load user full name
    [API doLoadUserFullname:user.userName completion:^(NSError * _Nullable error, id  _Nullable response) {
        NSLog(@"%@", response);
        
        if (error) {
            user.fullName = @"Não disponível";
        } else if (response) {
            if (![Utils isEmptyString:response[@"name"]]) {
                user.fullName = response[@"name"];
            } else {
                user.fullName = @"Não disponível";    
            }
            
            if (block) {
                block(user);
            }
        } else {
            user.fullName = @"Não disponível";
        }
    }];
}

@end
