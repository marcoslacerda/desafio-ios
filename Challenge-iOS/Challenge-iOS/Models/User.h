//
//  User.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSString *avatarURL;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *fullName;

+(void)initWithJSON:(NSDictionary *)json completion:(void (^)(User *))block;

@end
