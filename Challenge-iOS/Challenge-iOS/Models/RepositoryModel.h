//
//  RepositoryModel.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

typedef void (^ _Nonnull CompletionBlock) (id _Nonnull repo);

@interface RepositoryModel : NSObject

@property (strong, nonatomic) NSString * _Nonnull repoName;
@property (strong, nonatomic) NSString * _Nonnull repoDesc;
@property (assign, nonatomic) int repoForkCount;
@property (assign, nonatomic) int repoStarCount;
@property (strong, nonatomic) User * _Nonnull repoOwner;

+(void)initWithJSON:(NSDictionary * _Nonnull)json completion:(CompletionBlock)completionBlock;

@end
