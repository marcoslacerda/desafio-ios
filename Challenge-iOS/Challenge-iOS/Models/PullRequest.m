//
//  PullRequest.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "PullRequest.h"

@implementation PullRequest

@synthesize user, pullTitle, pullDate, pullBody, pullURL;

+(void)initWithJSON:(NSDictionary *)json completion:(CompletionBlock)block {
    PullRequest *pullRequest = [[PullRequest alloc] init];
    
    pullRequest.pullTitle = json[@"title"];
    pullRequest.pullDate = json[@"created_at"];
    pullRequest.pullBody = json[@"body"];
    pullRequest.pullURL = json[@"html_url"];
    
    [User initWithJSON:json[@"user"] completion:^(User *owner) {
        pullRequest.user = owner;
        
        if (block) {
            block(pullRequest);
        }
    }];
}

@end
