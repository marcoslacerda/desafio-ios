//
//  RepositoryModel.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "RepositoryModel.h"

@implementation RepositoryModel

@synthesize repoName, repoDesc, repoForkCount, repoStarCount, repoOwner;

+(void)initWithJSON:(NSDictionary *)json completion:(CompletionBlock)completionBlock {
    RepositoryModel *repository = [[RepositoryModel alloc] init];
    
    repository.repoName = json[@"name"];
    repository.repoDesc = json[@"description"];
    repository.repoForkCount = [json[@"forks_count"] intValue];
    repository.repoStarCount = [json[@"stargazers_count"] intValue];
    
    [User initWithJSON:json[@"owner"] completion:^(User *owner) {
        if (completionBlock) {
            repository.repoOwner = owner;
            completionBlock(repository);
        }
    }];
}

@end
