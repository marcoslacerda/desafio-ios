//
//  PullRequest.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

typedef void (^ _Nonnull CompletionBlock) (id _Nonnull pullRequest);

@interface PullRequest : NSObject

@property (strong, nonatomic) User * _Nonnull user;
@property (strong, nonatomic) NSString * _Nonnull pullTitle;
@property (strong, nonatomic) NSString *  _Nonnull pullDate;
@property (strong, nonatomic) NSString * _Nonnull pullBody;
@property (strong, nonatomic) NSString * _Nonnull pullURL;

+(void)initWithJSON:(NSDictionary * _Nonnull)json completion:(CompletionBlock)block;

@end
