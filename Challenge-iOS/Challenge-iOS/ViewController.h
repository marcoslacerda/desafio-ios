//
//  ViewController.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoryModel.h"

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *repositoriesTableView;
    
    int page;
    int totalRequested;
    NSMutableArray<RepositoryModel *> *items;
}


@end

