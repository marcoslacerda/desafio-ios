//
//  RepositoriesTableViewCell.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "RepositoriesTableViewCell.h"

@implementation RepositoriesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Inital Configs
    userPhoto.layer.cornerRadius = userPhotoWidthConstraint.constant / 2;
    userPhoto.layer.masksToBounds = YES;
    
    placeHolder = [[UIImage imageNamed:@"ic-place-holder"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

-(void)initWithItem:(RepositoryModel *)repo {
    repositoryName.text = repo.repoName;
    repositoryDescription.text = repo.repoDesc;
    
    forksCount.text = [NSString stringWithFormat:@"%i", repo.repoForkCount];
    startsCount.text = [NSString stringWithFormat:@"%i", repo.repoStarCount];
    
    if (![Utils isEmptyString:repo.repoOwner.avatarURL]) {
        NSURL *avatarURL = [NSURL URLWithString:repo.repoOwner.avatarURL];
        
        [userPhoto sd_setImageWithURL:avatarURL placeholderImage:placeHolder];
    } else {
        userPhoto.image = placeHolder;
    }
    
    userName.text = repo.repoOwner.userName;
    userFullName.text = repo.repoOwner.fullName;
    
    [repositoryName sizeToFit];
    [repositoryDescription sizeToFit];
    [forksCount sizeToFit];
    [startsCount sizeToFit];
}

@end
