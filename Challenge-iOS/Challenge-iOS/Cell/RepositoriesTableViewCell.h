//
//  RepositoriesTableViewCell.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoryModel.h"

@interface RepositoriesTableViewCell : UITableViewCell {
    IBOutlet UILabel *repositoryName;
    IBOutlet UILabel *repositoryDescription;
    IBOutlet UILabel *forksCount;
    IBOutlet UILabel *startsCount;
    IBOutlet UIImageView *userPhoto;
    IBOutlet UILabel *userName;
    IBOutlet UILabel *userFullName;
    IBOutlet NSLayoutConstraint *userPhotoWidthConstraint;
    
    UIImage *placeHolder;
}

-(void)initWithItem:(RepositoryModel *)repo;

@end
