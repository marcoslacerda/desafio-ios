//
//  RepoDetailViewController.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "RepoDetailViewController.h"
#import "RepoDetailTableViewCell.h"
#import <SVPullToRefresh/SVPullToRefresh.h>

@interface RepoDetailViewController ()

@end

@implementation RepoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    
    self.title = _repoDetail.repoName;
    page = 1;
    totalRequested = 0;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadPullRequests:YES];
}

#pragma mark - UI

-(void)initUI {
    [self configureTableView];
}

-(void)configureTableView {
    [pullRequestsTableView registerNib:[UINib nibWithNibName:@"RepoDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"PullRequestsCell"];
    
    pullRequestsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    pullRequestsTableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
    
    pullRequestsTableView.rowHeight = UITableViewAutomaticDimension;
    pullRequestsTableView.estimatedRowHeight = 400;
    
    // Configure Infinite Scroll
    __block id blockSelf = self;
    
    [pullRequestsTableView addInfiniteScrollingWithActionHandler:^{
        page = page + 1;
        
        [blockSelf loadPullRequests:NO];
    }];
    
    pullRequestsTableView.delegate = self;
    pullRequestsTableView.dataSource = self;
}

#pragma mark - Actions

-(void)loadPullRequests:(BOOL)showProgress {
    if (!items) {
        items = [[NSMutableArray alloc] init];
    }
    
    if (showProgress) {
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }
    
    [API doListPullRequests:_repoDetail.repoOwner.userName repoName:_repoDetail.repoName page:page completion:^(NSError * _Nullable error, id  _Nullable response) {
        if (error) {
            [self hideProgress:showProgress];
            [Utils showMessage:self.navigationController message:error.localizedDescription];
        } else if (response) {
            totalRequested += [response count];
            
            if ([response count] == 0) {
                [pullRequestsTableView reloadData];
            }
            
            for (NSDictionary *item in response) {
                [PullRequest initWithJSON:item completion:^(id  _Nonnull pull) {
                    if (![items containsObject:pull]) {
                        [items addObject:pull];
                    }
                    
                    if (items.count == totalRequested) {
                        [self hideProgress:showProgress];
                        
                        [pullRequestsTableView reloadData];
                    }
                }];
            }
        } else {
            [self hideProgress:showProgress];
            [Utils showMessage:self.navigationController message:@"An error occurred to load pull requests."];
        }
    }];
}

-(void)hideProgress:(BOOL)performHideProgress {
    if (performHideProgress) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    }
}

-(void)clearItems {
    [items removeAllObjects];
    items = nil;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView.infiniteScrollingView stopAnimating];
    
    RepoDetailTableViewCell *cell = (RepoDetailTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"PullRequestsCell"];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell initWithItem:items[indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequest *pullRequest = items[indexPath.row];
    
    NSURL *url = [NSURL URLWithString:pullRequest.pullURL];
    
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
