//
//  RepoDetailViewController.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"
#import "RepositoryModel.h"

@interface RepoDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *pullRequestsTableView;
    
    int page;
    int totalRequested;
    NSMutableArray<PullRequest *> *items;
}

@property (strong, nonatomic) RepositoryModel *repoDetail;

@end
