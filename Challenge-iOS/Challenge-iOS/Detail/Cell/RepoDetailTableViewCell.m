//
//  RepoDetailTableViewCell.m
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import "RepoDetailTableViewCell.h"

@implementation RepoDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Inital Configs
    userPhoto.layer.cornerRadius = userPhotoWidthConstraint.constant / 2;
    userPhoto.layer.masksToBounds = YES;
    
    placeHolder = [[UIImage imageNamed:@"ic-place-holder"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}

-(void)initWithItem:(PullRequest *)pullRequest {
    pullRequestDate.text = pullRequest.pullDate;
    
    pullRequestTitle.text = pullRequest.pullTitle;
    pullRequestDesc.text = pullRequest.pullBody;
    
    if (![Utils isEmptyString:pullRequest.user.avatarURL]) {
        NSURL *avatarURL = [NSURL URLWithString:pullRequest.user.avatarURL];
        
        [userPhoto sd_setImageWithURL:avatarURL placeholderImage:placeHolder];
    } else {
        userPhoto.image = placeHolder;
    }
    
    userName.text = pullRequest.user.userName;
    userFullName.text = pullRequest.user.fullName;
    
    [pullRequestTitle sizeToFit];
    [pullRequestDesc sizeToFit];
}

@end
