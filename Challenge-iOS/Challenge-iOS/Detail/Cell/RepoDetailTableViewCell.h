//
//  RepoDetailTableViewCell.h
//  Challenge-iOS
//
//  Created by Marcos Lacerda on 23/01/17.
//  Copyright © 2017 Marcos Lacerda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"

@interface RepoDetailTableViewCell : UITableViewCell {
    IBOutlet UILabel *pullRequestDate;
    IBOutlet UILabel *pullRequestTitle;
    IBOutlet UILabel *pullRequestDesc;
    IBOutlet UIImageView *userPhoto;
    IBOutlet UILabel *userName;
    IBOutlet UILabel *userFullName;
    IBOutlet NSLayoutConstraint *userPhotoWidthConstraint;
    
    UIImage *placeHolder;
}

-(void)initWithItem:(PullRequest *)pullRequest;

@end
